<!DOCTYPE html>
<html>
<head>
    <title>Image Resizing - CRUD FORM</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/jquery.js') }}"></script>  
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />

</head>
<body>

<div class="container">
 <div class="table-wrapper">
    <div class="table-title">
        <div class="row">
            <div class="col-sm-6">
                <h2>Manage <b> Resized Images</b></h2>
            </div>
            <div class="col-sm-6">
                <a href="#" class="btn btn-success" id ="imgResize"><i class="material-icons">&#xE147;</i> <span>Resize Image</span></a>                    
            </div>
        </div>
    </div>
    <table class="table table-striped table-hover data-table">
         <thead>
            <tr>
                <th></th>
                <th>Resized URL</th>
                <th>Width</th>
                <th>Height</th>
                <th width="280px">Action</th>
            </tr>
         </thead>
        <tbody>
        </tbody>
    </table>
   </div>
 </div>

   
<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
           <form id="imgForm" name="imgForm" class="form-horizontal">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading">Resize image and store it!</h4>
            </div>
            <div class="modal-body">

        <div class="alert alert-danger" style="display:none"></div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">URL</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="image" name="image" placeholder="Enter path of Image" value="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Width</label>
                        <div class="col-sm-12">
                            <input type="number"  id="width" name="width"  class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Height</label>
                        <div class="col-sm-12">
                            <input type="number" id="height" name="height"  class="form-control">
                        </div>
                    </div>
                
              </div>
              <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                <input type="submit" class="btn btn-info" value="Save" id="saveBtn">
              </div>
            </form>
        </div>
    </div>
</div>

</body>

    
<script type="text/javascript">

  $(function () {

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('imgService.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'image_resized', name: 'image_resized'},
            {data: 'width', name: 'width'},
            {data: 'height', name: 'height'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });


    $('#imgResize').click(function () {
        $('#imgForm').trigger("reset");
        $('.alert-danger').html('');
        $('.alert-danger').hide();
        $('#ajaxModel').modal('show');
    });

    
    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $('.alert-danger').html('');
        $(this).html('Sending..');
        $.ajax({
          data: $('#imgForm').serialize(),
          url: "{{ route('imgService.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              $('#imgForm').trigger("reset");
              $('#ajaxModel').modal('hide');
              $('.alert-danger').hide();
              table.draw();
          },
          error: function (data) {
              var result = JSON.parse(data.responseText);
              var message = result.message;
              $.each(message, function(key, value) {
                 $('.alert-danger').show();
                 $('.alert-danger').append('<li>'+value+'</li>');
              });
              $('#saveBtn').html('Save Changes');
          }
      });
    });

  $('body').on('click', '.deleteImage', function () {
        var id = $(this).data("id");
        if(confirm("Are you sure want to delete!"))
        {

        $.ajax({
             type: "DELETE",
             url: '/imgService/'+id,
             success: function (data) {
                table.draw();
             },
             error: function (data) {
                alert('Image could not be deleted!');
             }
            });
        }
    });
  });
</script>

</html>