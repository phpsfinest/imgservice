<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use App\ImageResized;
use DataTables;
use Validator;
use Storage;


class imgResizeController extends Controller
{

    /**
     * Display a listing of the images.
     *
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = ImageResized::latest()->get();

            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                           $btn = '<a href="'.$row->image_resized.'" target="_blank" class="showLink"> <i class=" material-icons" ata-toggle="tooltip" title="Show Image">remove_red_eye</i></a>';
                           $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'"  class="deleteImage"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>';

                            return $btn;

                    })
                    ->rawColumns(['action'])
                    ->make(true);

        }
        return view('images',compact('images_resized'));
    }

    /**
     * Store a newly resized image in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input         = $request->all();
        $validator     = Validator::make($input, [
            'image'    => 'required|url|unique:images_resized',
            'width'    => 'required',
            'height'   => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data'    => 'Validation Error.',
                'message' => $validator->errors()->all()
            ];
            return response()->json($response, 404);
        }
        
        $input['image_resized']  = $this->ImageResizedUrl($request['image'], array($input['width'], $input['height']));

        $image = ImageResized::create($input);
        $data  = $image->toArray();

        $response = [
            'success' => true,
            'message' => 'Image resized successfully.'
        ];

        return response()->json($response, 200);
    }

   /**
     * Remove the specified resource from storage.
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $img = ImageResized::find($id);
        $imgName = basename($img['image_resized']);
        $img->delete();
        Storage::disk('public')->delete($imgName);
        return response()->json(['success' => true,'message'=>'Image deleted successfully.']);
    }


    /**
     * Resize image and store in storage.
     * @return url
     */
    private function ImageResizedUrl($url, array $dimension)
    {
        $photo        = file_get_contents($url);
        $timestamp    = microtime(true) * 10000;
        $extension    = pathinfo($url);
        $imageName    = 'img_resized_' . $timestamp . '.' . $extension['extension'];
		$image_resize = Image::make($photo)->resize($dimension[0], $dimension[1], function ($constraint) {
		    $constraint->aspectRatio();
		})->encode($extension['extension']);

		Storage::disk('public')->put($imageName, (string)$image_resize);

        return url('/storage/'. $imageName);
    }
    
}
