<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Intervention\Image\ImageManagerStatic as Image;
use App\ImageResized;
use Validator;
use Storage;


class ImageController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = ImageResized::all();
        $data   = $images->toArray();

        $response = [
            'success' => true,
            'data'    => $data,
            'message' => 'Images retrieved successfully.'
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly resized image in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input         = $request->all();
        $validator     = Validator::make($input, [
            'image'    => 'required|url|unique:images_resized',
            'width'    => 'required',
            'height'   => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data'    => 'Validation Error.',
                'message' => $validator->errors()
            ];
            return response()->json($response, 404);
        }
        
        $input['image_resized']  = $this->ImageResizedUrl($request['image'], array($input['width'], $input['height']));

        $image = ImageResized::create($input);
        $data  = $image->toArray();

        $response = [
            'success' => true,
            'data'    => $data,
            'message' => 'Image resized successfully.'
        ];

        return response()->json($response, 200);
    }

    /**
     * Display the specified image.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $image = ImageResized::find($id);
        $data  = $image->toArray();

        if (is_null($image)) {
            $response = [
                'success' => false,
                'data'    => 'Empty',
                'message' => 'Image not found.'
            ];
            return response()->json($response, 404);
        }


        $response = [
            'success' => true,
            'data'    => $data,
            'message' => 'Image retrieved successfully.'
        ];

        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function deleteByUrl(Request $request)
    {
        try {
		    $image = ImageResized::where('image_resized', '=', $request->url)->firstOrFail();
            $imgName = basename($image['image_resized']);
            $image->delete();
            Storage::disk('public')->delete($imgName);
		    $response = [
		            'success' => true,
		            'message' => 'Image deleted successfully.'
		    ];
		    $status = 204;
		} catch(ModelNotFoundException $e) {
		    $response = [
		            'success' => false,
		            'message' => 'URL not exists!'
		    ];
		    $status = 404;
		}
        return response()->json($response, $status);
    }

   /**
     * Resize image and store in storage.
     * @return url
     */
    private function ImageResizedUrl($url, array $dimension)
    {
        $photo        = file_get_contents($url);
        $timestamp    = microtime(true) * 10000;
        $extension    = pathinfo($url);
        $imageName    = 'img_resized_' . $timestamp . '.' . $extension['extension'];
		$image_resize = Image::make($photo)->resize($dimension[0], $dimension[1], function ($constraint) {
		    $constraint->aspectRatio();
		})->encode($extension['extension']);

		Storage::disk('public')->put($imageName, (string)$image_resize);

        return url('/storage/'. $imageName);
    }

}
