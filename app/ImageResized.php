<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageResized extends Model
{
    /**
     * The table.
     *
     * @var string
     */

	protected $table = "images_resized";


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'image', 'image_resized','width', 'height'
    ];
}
