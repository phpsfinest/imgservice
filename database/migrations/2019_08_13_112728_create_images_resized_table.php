<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesResizedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images_resized', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image');
            $table->string('image_resized');
            $table->integer('width');
            $table->integer('height');
            $table->enum('property', ['%', 'px'])->default('px');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images_resized');
    }
}
